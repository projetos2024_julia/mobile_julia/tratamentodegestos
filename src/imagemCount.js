import React, { useRef, useState } from "react";
import { View, Dimensions, PanResponder, Image, StyleSheet, Text } from "react-native";

const ImagemCount = () => {
    const [count, setCount] = useState(0);
    const images = [
        require('../assets/imagem1.jpg'),
        require('../assets/imagem2.jpg'),
        require('../assets/imagem3.jpg')
    ]; 
    const screenWidth = Dimensions.get("window").width;
    const gestureThreshold = screenWidth * 0.25;

    const panResponder = useRef(
        PanResponder.create({
            onStartShouldSetPanResponder: () => true,
            onPanResponderMove: () => {},
            onPanResponderRelease: (event, gestureState) => {
                
                if (gestureState.dx < -gestureThreshold) {
                    setCount((prevCount) => prevCount + 1); // Avança para a próxima imagem
                } 
                else if (gestureState.dx > gestureThreshold) {
                    setCount((prevCount) => prevCount - 1 ); // Volta para a imagem anterior
                }
            },
        })
    ).current;

    return (
        <View {...panResponder.panHandlers} style={styles.container}>
            <View style={styles.imageContainer}>
                <Image source={images[count]} style={styles.image} />
                <Text style={styles.imageNumber}>{count + 1}</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: "center"
    },
    imageContainer: {
        position: 'relative',
    },
    image: {
        width: 200,
        height: 200,
        resizeMode: 'contain',
    },
    imageNumber: {
        position: 'absolute',
        top: 10,
        left: 10,
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
    },
});

export default ImagemCount;

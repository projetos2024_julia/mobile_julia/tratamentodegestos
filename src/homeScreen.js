import React from "react";

import { Button, View, StyleSheet } from "react-native";

const HomeScreen = ({ navigation }) => {
    return (
      <View style={styles.container}>
        <Button
          title="Ir para CountMoviment"
          onPress={() => navigation.navigate("CountMoviment")}
        />
        <Button
          title="Ir para ImagemCount"
          onPress={() => navigation.navigate("ImagemCount")}
        />
      </View>
    );
  };
  export default HomeScreen;

  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
    },
  });